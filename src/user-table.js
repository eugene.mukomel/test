import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './user-table.css';
import dataUser from './data/data-user';

class UserTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listItems: dataUser,
      name: '',
      city: ''
      
    };

    this.removeRow = this.removeRow.bind(this);
    this.edit = this.edit.bind(this);
    this.firstColum = this.firstColum.bind(this);
    this.secondColum = this.secondColum.bind(this);
    this.addRow = this.addRow.bind(this);
  }

  firstColum(event) {
    this.setState({name: event.target.value});
  }
  
  secondColum(event) {
    this.setState({city: event.target.value});
  }

  addRow(event) {
    event.preventDefault();
    const arr = this.state.listItems;
    const id = {
      name: this.state.name,
      city: this.state.city
    };

    arr.unshift(id);
    this.setState({
      listItems: arr
    });
  }

  removeRow(event) {
    const {id} = event.target.dataset;
    const arr = this.state.listItems.slice();

    arr.splice(id, 1);
    this.setState({
      listItems: arr
    });
  }

  getInitialState(){
    return {edit: false}
  }

  save = () => {
    /*
    var areaName = this.refs.areaName.value;
    var areaCity = this.refs.areaCity.value;
    
    alert(areaName + " " + areaCity);
    */
    this.setState ({edit: false})
  };

  edit = () => {
    this.setState ({edit: true});
  };

  getRow(data, index) {
    return(
      <tr key={index}>
        <td><input type="text" name="country" value={data.name} readOnly={this.state.readOnly} /></td>
        <td><input type="text" name="country" value={data.city} readOnly /></td>
        <td>
          <button
            data-id={index}
            onClick={this.removeRow}
            className="button-style"
          >Удалить запись</button>
        </td>
        <td>
          <button
            data-id={index}
            className="button-style"
            onClick={this.edit}
          >Редактировать запись</button>
        </td>  
      </tr>
    );
  }

  render() {
      return (
        <div>
          <table>
            <tbody>
              {this.state.listItems.map((row, index) => this.getRow(row, index))}
            </tbody>           
          </table>
          <div>
            <form onSubmit={this.addRow}>
            <label>
              <input type="text" value={this.state.name} onChange={this.firstColum} placeholder="Первый столбец"  />
            </label>
            <br/>
            <label>
              <input type="text" value={this.state.city} onChange={this.secondColum} placeholder="Второй столбец" />
            </label>
            <br/>
            <input type="submit" value="Добавить" />
          </form>
          </div>
        </div>
      ); 
    }
}



export default UserTable;