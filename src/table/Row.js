/* render component row */
import React, { Component } from 'react';

class Row extends Component {
    state = this.props.data.data

    onInput(type, event) {
        this.setState({
            [type]: event.target.value,
        });
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.isEdit === true && nextProps.isEdit === false) {
            this.setState(this.props.data.data);
        }
    }

    render() {
        const testData = this.props.data.data;
        const _id = this.props.data._id;
        const { isEdit } = this.props;
        const editBtn = isEdit ? 'Сохранить' : 'Редактировать';
        const dataInfo = isEdit ? this.state : testData;

        return (
            <tr>
                <td>
                    <input type="text" name="db" value={dataInfo.db} readOnly={!isEdit} onInput={this.onInput.bind(this, 'db')}/>
                </td>
                <td>
                    <input type="text" name="name" value={dataInfo.name} readOnly={!isEdit} onInput={this.onInput.bind(this, 'name')} />
                </td>
                <td>
                    <input type="text" name="surname" value={dataInfo.surname} readOnly={!isEdit} onInput={this.onInput.bind(this, 'surname')} />
                </td>
                <td>
                    <button 
                    className="button-style"
                    onClick={() => {
                           this.props.delData(_id);
                        }}
                    >Удалить запись</button>
                </td>
                <td>
                    <button
                        className="button-style"
                        onClick={() => {
                            return this.props.isEdit ? this.props.onSelectedGet(_id, dataInfo, isEdit) : this.props.onSelected(_id);
                        }}
                    >{editBtn}</button>
                </td>
            </tr>
        );
    }
}

export default Row;