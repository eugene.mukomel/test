import React from 'react';
import ReactDOM from 'react-dom';
import './user-table.css';
import dataUser from './data-user';

function UserTable(props) {
  const numbers = props.dataUser;
  const listItems = numbers.map((number, i) =>
  <tr key={i} index={i}>
  <td>{number.name}</td>
  <td>{number.city}</td>
  <td><p><button className="button-style">Удалить запись</button></p></td>
  <td><p><button className="button-style">Редактировать запись</button></p></td>  
 </tr>
  );
/*
function deleteUser(){
    const numbers = props.dataUser;
    let numberIndex = 0;
    for(var i = 0; i < numbers; i++){
        numberIndex = i;
    }
    const index = numbers.indexOf(numberIndex, 0);
 
    if (index > -1) {
      numbers.splice(numberIndex, 1);
    }
  };


function deleteUser(i){
  const arr = this.state.dataUser;
  arr.splice(i, 1);
  this.setState ({dataUser: arr});
};
*/

  return (
    <table>
      <td>{listItems}</td>
      <p><button className="button-style">Добавить запись</button></p>
    </table>
  );
}

ReactDOM.render(
  <UserTable dataUser={dataUser} />,
  document.getElementById('root')
);

export default UserTable;